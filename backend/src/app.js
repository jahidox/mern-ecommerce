const express = require('express');
const app = express();

// settings
app.set('port', process.env.PORT);

// middlewares
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// routes
const authRoutes = require('./routes/auth');
const adminRoutes = require('./routes/admin/auth');
const categoryRoutes = require('./routes/category');

app.use('/api', authRoutes);
app.use('/api', adminRoutes);
app.use('/api', categoryRoutes);

module.exports = app;
