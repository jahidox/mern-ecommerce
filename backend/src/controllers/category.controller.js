const Category = require('../models/category.model');
const slugify = require('slugify');

const addCategory = (req, res) => {
  const { name, parentId } = req.body;

  const categoryObj = {
      name,
      slug: slugify(name)
  }

  if(parentId){
    categoryObj.parentId = parentId;
  }

  const category = new Category(categoryObj);

  category.save((error, data) => {
    if (error) {
      return res.status(400).json({
        ok: false,
        message: "Something went wrong",
      });
    }

    if (data) {
      return res.status(201).json({
        ok: true,
        message: "Category created Successfully..!",
      });
    }
  });

};

function createCategories(categories, parentId = null){
  const categoryList = [];
  let category;
  if (parentId == null){
    category = categories.filter(cat => cat.parentId == undefined)
  } else {
    category = categories.filter(cat => cat.parentId == parentId)
  }

  for (let cate of category){
    categoryList.push({
      _id: cate._id,
      name: cate.name,
      slug: cate.slug,
      children: createCategories(categories, cate._id)
    })
  }

  return categoryList;
}

const getCategories = (req, res) => {
  Category.find({})
  .exec( (error, data) => {
    if (error) {
      return res.status(400).json({
        ok: false,
        error
      });
    }

    if (data) {
      const categoryList = createCategories(data);
      return res.status(201).json({
        ok: true,
        message: "Data obtained successfully..!",
        data: categoryList
      });
    }
  })
};

module.exports = {
  addCategory,
  getCategories
}
