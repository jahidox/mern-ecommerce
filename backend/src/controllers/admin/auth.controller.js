const User = require("../../models/user.model");
const jwt = require('jsonwebtoken');

const signUp = (req, res) => {
  let { email } = req.body;

  User.findOne({ email })
  .exec((error, user) => {
    if (user)
      return res.status(400).json({
        ok: false,
        message: "Admin already registered",
      });

    const { firstName, lastName, email, password } = req.body;
    const _user = new User({
      firstName,
      lastName,
      email,
      password,
      username: Math.random().toString(),
      role: 'admin'
    });

    _user.save((error, data) => {
      if (error) {
        return res.status(400).json({
          ok: false,
          message: "Something went wrong",
        });
      }

      if (data) {
        return res.status(201).json({
          ok: true,
          message: "Admin created Successfully..!",
        });
      }
    });

  });
}

const signIn = (req, res) => {
  let { email, password } = req.body;

  User.findOne({ email }).exec((error, user) => {
    if (error){
      return res.status(400).json({
        ok: false,
        error
      });
    }

    if (user) {

      if (user.authenticate(password) && user.role === 'admin') {
        const token = jwt.sign({ _id: user._id, role: user.role }, process.env.JWT_SECRET, {
          expiresIn: process.env.TOKEN_EXPIRY,
        });
        const { _id, firstName, lastName, email, role, fullName } = user;

        res.status(200).json({
          ok: true,
          token,
          user: { _id, firstName, lastName, email, role, fullName },
        });

      } else {
        return res.status(400).json({
          ok: false,
          message: "Invalid Password",
        });
      }

    } else {
      return res.status(400).json({
        ok: false,
        message: "Something went wrong"
      });
    }
  });
};

module.exports = {
  signUp,
  signIn,
}
