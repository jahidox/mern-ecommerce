const { Router } = require("express");
const router = Router();
const { addCategory, getCategories } = require("../controllers/category.controller");
const { requireSignin, verifyAdminRole } = require("../middlewares/authentication");

router.post("/category/create", [requireSignin, verifyAdminRole], addCategory);
router.get("/category/get", getCategories);

module.exports = router;
