// ===================================================
// Port
// ===================================================
process.env.PORT = process.env.PORT || 4000;

// ===================================================
// Environment
// ===================================================
process.env.NODE_ENV = process.env.NODE_ENV || 'dev'

// ===================================================
// Database
// ===================================================
let urlDB;
if(process.env.NODE_ENV === 'dev'){
  urlDB = 'mongodb://localhost/mern-ecommerce'
} else {
  urlDB = process.env.MONGODB_URI
}
process.env.URLDB = urlDB

// ===================================================
// Token expiry
// ===================================================
process.env.TOKEN_EXPIRY = '48h';

// ===================================================
// Seed
// ===================================================
process.env.JWT_SECRET = process.env.JWT_SECRET || 'seed-development';
