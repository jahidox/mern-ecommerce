require('./config/config');
const env = require('dotenv');

//environment variable or you can say constants
env.config();

const app = require('./app');
require('./connection.database');

app.listen(app.get('port'), () => {
  console.log('Server on port', app.get('port'));
})
